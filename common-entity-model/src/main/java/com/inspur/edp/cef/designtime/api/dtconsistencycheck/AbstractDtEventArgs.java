/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.dtconsistencycheck;

import io.iec.edp.caf.commons.event.CAFEventArgs;
import java.util.ArrayList;
import java.util.List;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class AbstractDtEventArgs extends CAFEventArgs {

  public List<ConsistencyCheckEventMessage> eventMessages;

  public AbstractDtEventArgs() {
    eventMessages = new ArrayList<>();
  }


  public void addEventMessage(ConsistencyCheckEventMessage eventMessage) {
    this.eventMessages.add(eventMessage);
  }

  public List<ConsistencyCheckEventMessage> getEventMessages() {
    return eventMessages;
  }

  public void setEventMessages(
      List<ConsistencyCheckEventMessage> eventMessages) {
    this.eventMessages = eventMessages;
  }

}
