/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildValTriggerInfo;
import java.util.HashMap;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;
import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

public class CommonChildValTriggerInfoDeserializer extends JsonDeserializer<HashMap<String, ChildValTriggerInfo>> {
    private String curChildCode = "";
    @Override
    public HashMap<String, ChildValTriggerInfo> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        HashMap<String, ChildValTriggerInfo> hashMap = new HashMap<>();
        if (SerializerUtils.readNullObject(jsonParser)) {
            return null;
        }
        SerializerUtils.readStartArray(jsonParser);
        JsonToken tokentype = jsonParser.getCurrentToken();
        if (tokentype != END_ARRAY) {
            while (jsonParser.getCurrentToken() == tokentype) {
                readHashMap(hashMap, jsonParser);
            }
        }
        SerializerUtils.readEndArray(jsonParser);
        return hashMap;
    }

    private void readHashMap(HashMap<String, ChildValTriggerInfo> hashMap, JsonParser parser) {
        SerializerUtils.readStartObject(parser);
        while (parser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(parser);
            readPropertyValue(hashMap, propName, parser);
        }
        SerializerUtils.readEndObject(parser);
    }

    private void readPropertyValue(HashMap<String, ChildValTriggerInfo> hashMap, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.RequestChildCode:
                curChildCode = SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            case CefNames.RequestDtmChildElementValue:
            {
                ChildValTriggerInfoDeserializer childTriggerInfoDeserializer = new ChildValTriggerInfoDeserializer();
                ChildValTriggerInfo childValTriggerInfo = childTriggerInfoDeserializer.deserialize(jsonParser, null);
                hashMap.put(curChildCode, childValTriggerInfo);
            }
            break;
        }
    }
}
