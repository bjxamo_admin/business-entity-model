/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.validate.common;

import java.util.Arrays;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CheckUtil {
    public static boolean isLegality(String code) {
        if (code == null || code.isEmpty())
            return false;
        return Arrays.binarySearch(Keywords.javaKeyworks, code) < 0;
    }

    /**
     * 判断字符串只包含字母、数字、下划线
     * @param value
     * @return
     */
    public static boolean isValidInput(String value){
        boolean result = true;
        return result;
    }

    /**
     * 是否字母开头
     * @param value
     * @return
     */
    public static boolean isStartWithChar(String value){
        return Character.isLetter(value.charAt(0));
    }

    public static void exception(String message) {
        if (message == null || message.isEmpty())
            return;
        throw new RuntimeException(message);
    }
}
