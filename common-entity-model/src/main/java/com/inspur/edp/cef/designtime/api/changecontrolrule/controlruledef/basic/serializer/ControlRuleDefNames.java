/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer;

/**
 * The Serializer Names Of ControlRuleDef
 *
 * @ClassName: ControlRuleDefNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleDefNames {
    public static final String RuleObjectType="RuleObjectType";
    public static final String SelfControlRules="SelfControlRules";
    public static final String RangeControlRules="RangeControlRules";
    public static final String Description="Description";
    public static final String RuleDisplayName="RuleDisplayName";
    public static final String RuleName="RuleName";
    public static final String DefaultRuleValue="DefaultRuleValue";
    public static final String ChildControlRules="ChildControlRules";
}
