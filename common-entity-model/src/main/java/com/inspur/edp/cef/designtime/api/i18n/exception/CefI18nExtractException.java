/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.i18n.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class CefI18nExtractException extends CAFRuntimeException {

    private static String i18nExceptionCode = "CefI18nExtractException";

    public CefI18nExtractException(
        String exceptionCode, String resourceFile, String messageCode, Exception innerException, String[] exceptionParams) {
        this(exceptionCode, resourceFile, messageCode, innerException, exceptionParams, ExceptionLevel.Error, false);
    }

    public CefI18nExtractException(
        String exceptionCode, String resourceFile, String messageCode, Exception innerException, String[] exceptionParams, ExceptionLevel level) {
        this(exceptionCode, resourceFile, messageCode, innerException, exceptionParams, level, false);
    }

    public CefI18nExtractException(
        String exceptionCode, String resourceFile, String messageCode, Exception innerException, String[] exceptionParams, ExceptionLevel level, boolean isBizException) {
        super(exceptionCode, resourceFile, messageCode, exceptionParams, innerException, level, isBizException);
    }

//		public CefI18nExtractException(String exceptionCode, String exceptionMessage, System.Exception innerException = null, ExceptionLevel level = ExceptionLevel.Error, bool isBizException = false) implements base(exceptionCode, exceptionMessage, innerException, level, isBizException)
//		{
//		}

//		protected CefI18nExtractException(SerializationInfo info, StreamingContext context) implements base(info, context)
//		{
//		}

//		public CefI18nExtractException(String exceptionMessage)
//		{
//			super(i18nExceptionCode, exceptionMessage);
//		}
}

