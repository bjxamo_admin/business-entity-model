/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.element.increment;

import com.inspur.edp.cef.designtime.api.increment.DeletedIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DeletedFieldIncrement extends GspCommonFieldIncrement implements DeletedIncrement {

    private String deleteId;

    public DeletedFieldIncrement(String deleteId) {
        this.deleteId = deleteId;
        setId(deleteId);
    }

    public String getDeleteId() {
        return deleteId;
    }

    @Override
    public final IncrementType getIncrementType() {
        return IncrementType.Deleted;
    }
}
