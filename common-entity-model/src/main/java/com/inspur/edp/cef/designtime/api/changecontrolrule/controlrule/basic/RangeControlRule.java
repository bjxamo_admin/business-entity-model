/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic;

import java.util.HashMap;
import java.util.Map;

/**
 * The Definition Of RangeControlRule
 *
 * @ClassName: RangeControlRule
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RangeControlRule extends AbstractControlRule {

    private Map<String, Map<String, ControlRuleItem>> rangeControlRules = new HashMap<>();

    public Map<String, Map<String, ControlRuleItem>> getRangeControlRules() {
        return rangeControlRules;
    }

    public ControlRuleItem getRangeControlRuleItem(String ruleObjectType, String ruleName) {
        if (getRangeControlRules().containsKey(ruleObjectType) == false)
            return null;
        Map<String, ControlRuleItem> objectRules = getRangeControlRules().get(ruleObjectType);
        if (objectRules == null || objectRules.containsKey(ruleName) == false)
            return null;
        return objectRules.get(ruleName);
    }

    public void setRangeControlRules(String ruleObjectType, String ruleName, ControlRuleItem ruleItem) {
        if (getRangeControlRules().containsKey(ruleObjectType) == false) {
            Map<String, ControlRuleItem> map = new HashMap<>();
            getRangeControlRules().put(ruleObjectType, map);
        }
        Map<String, ControlRuleItem> objectRules = getRangeControlRules().get(ruleObjectType);
        objectRules.put(ruleName, ruleItem);
    }
}
