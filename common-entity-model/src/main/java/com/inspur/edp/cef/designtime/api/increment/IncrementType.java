/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.api.increment;

import java.util.HashMap;

public enum IncrementType {
    Added(0),
    Modify(1),
    Deleted(2);

    private int intValue;
    private static HashMap<Integer, IncrementType> mappings;
    private synchronized static HashMap<Integer, IncrementType> getMappings() {
        if (mappings == null) {
            mappings = new HashMap();
        }
        return mappings;
    }

    private IncrementType(int value) {
        intValue = value;
        IncrementType.getMappings().put(value, this);
    }

    public int getValue()
    {
        return intValue;
    }

    public static IncrementType forValue(int value)
    {
        return getMappings().get(value);
    }

}
