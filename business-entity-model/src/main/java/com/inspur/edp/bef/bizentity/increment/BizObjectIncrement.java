/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.increment;

import com.inspur.edp.bef.bizentity.increment.entity.determination.DtmIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import java.util.HashMap;

public class BizObjectIncrement extends ModifyEntityIncrement {

  private HashMap<String, DtmIncrement> determinations = new HashMap<>();

  public HashMap<String, DtmIncrement> getDeterminations() {
    return determinations;
  }

  private HashMap<String, ValIncrement> validations = new HashMap<>();

  public HashMap<String, ValIncrement> getValidations() {
    return validations;
  }

}
