/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.pushchangesetargs;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import java.util.Map;

public class ActionChangeDetail {

  private String actionId;
  private String actionCode;
  private boolean isNecessary;
  private Map<String, Object> changeInfo;
  private BizMgrAction mgrAction;
  private String parameterId;
  private String parameterCode;

  public String getParameterId() {
    return parameterId;
  }

  public void setParameterId(String parameterId) {
    this.parameterId = parameterId;
  }

  public String getParameterCode() {
    return parameterCode;
  }

  public void setParameterCode(String parameterCode) {
    this.parameterCode = parameterCode;
  }


  public Map<String, Object> getChangeInfo() {
    return changeInfo;
  }

  public void setChangeInfo(Map<String, Object> changeInfo) {
    this.changeInfo = changeInfo;
  }


  public ActionChangeDetail() {
  }

  public String getActionId() {
    return actionId;
  }

  public String getActionCode() {
    return actionCode;
  }

  public boolean isNecessary() {
    return isNecessary;
  }

  public BizMgrAction getMgrAction() {
    return mgrAction;
  }


  public void setActionId(String actionId) {
    this.actionId = actionId;
  }

  public void setActionCode(String actionCode) {
    this.actionCode = actionCode;
  }

  public void setNecessary(boolean necessary) {
    isNecessary = necessary;
  }

  public void setMgrAction(BizMgrAction mgrAction) {
    this.mgrAction = mgrAction;
  }

}
