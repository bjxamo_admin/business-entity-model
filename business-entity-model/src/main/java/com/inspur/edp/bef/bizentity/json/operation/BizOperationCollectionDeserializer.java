/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.BizOperationCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

public abstract class BizOperationCollectionDeserializer<T extends BizOperationCollection> extends
    JsonDeserializer<T> {

  @Override
  public T deserialize(JsonParser parser, DeserializationContext deserializationContext) {
    T collection = createCollection();
    if (SerializerUtils.readNullObject(parser)) {
      return collection;
    }
    SerializerUtils.readStartArray(parser);
    JsonToken tokentype = parser.getCurrentToken();
    if (tokentype != END_ARRAY) {
      while (parser.getCurrentToken() == tokentype) {
        BizOperation op = readBizOperation(collection, parser);
        addBizOp(collection, op);
      }
    }
    SerializerUtils.readEndArray(parser);
    return collection;
  }

  private BizOperation readBizOperation(T hashMap, JsonParser parser) {
    BizOperationDeserializer deserializer = createDeserializer();
    return deserializer.deserialize(parser, null);
  }

  protected void addBizOp(T collection, BizOperation op) {
    collection.add(op);
  }

  protected abstract T createCollection();

  protected abstract BizOperationDeserializer createDeserializer();
}
