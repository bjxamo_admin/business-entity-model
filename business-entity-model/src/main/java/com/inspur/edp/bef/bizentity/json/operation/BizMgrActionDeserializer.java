/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParamCollection;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

public class BizMgrActionDeserializer extends BizActionBaseDeserializer {

  @Override
  protected final boolean readExtendActionProperty(JsonParser jsonParser, BizOperation op,
      String propName) {
    BizMgrAction action = (BizMgrAction) op;
    boolean hasProperty = true;
    switch (propName) {
      case BizEntityJsonConst.FuncOperationID:
        action.setFuncOperationID(SerializerUtils.readPropertyValue_String(jsonParser));
        break;
      case BizEntityJsonConst.FuncOperationName:
        action.setFuncOperationName(SerializerUtils.readPropertyValue_String(jsonParser));
        break;
      default:
        hasProperty = false;
        break;
    }
    return hasProperty;
  }

  @Override
  protected BizActionBase createBizActionBase() {
    return new BizMgrAction();
  }

  @Override
  protected BizParaDeserializer createPrapConvertor() {
    return new BizMgrActionParaDeserializer();
  }

  @Override
  protected BizParameterCollection createPrapCollection() {
    return new BizMgrActionParamCollection();
  }
}
