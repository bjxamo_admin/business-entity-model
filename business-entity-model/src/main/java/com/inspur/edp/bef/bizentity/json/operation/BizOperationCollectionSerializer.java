/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.operation.BizOperationCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.io.IOException;

public abstract class BizOperationCollectionSerializer<T extends BizOperationCollection> extends
    JsonSerializer<T> {

  protected boolean isFull = true;

  public BizOperationCollectionSerializer() {
  }

  public BizOperationCollectionSerializer(boolean full) {
    isFull = full;
  }

  @Override
  public void serialize(BizOperationCollection value, JsonGenerator gen,
      SerializerProvider serializers) throws IOException {
    if (!isFull && value.size() == 0) {
      return;
    }
    writeBaseProperty(value, gen);
  }

  private void writeBaseProperty(BizOperationCollection operations, JsonGenerator writer) {
    if (operations.size() == 0) {
      SerializerUtils.WriteStartArray(writer);
      SerializerUtils.WriteEndArray(writer);
      return;
    }
    SerializerUtils.WriteStartArray(writer);
    for (int i = 0; i < operations.size(); i++) {
      getConvertor().serialize(operations.get(i), writer, null);
    }
    SerializerUtils.WriteEndArray(writer);
  }

  protected abstract BizOperationSerializer getConvertor();
}
