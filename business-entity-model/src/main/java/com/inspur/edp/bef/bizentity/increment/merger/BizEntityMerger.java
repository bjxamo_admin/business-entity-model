/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.increment.merger;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.BECategory;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.increment.BizEntityIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.AddedMgrActionIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.MgrActionIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.ModifyMgrActionIncrement;
import com.inspur.edp.cef.designtime.api.increment.merger.MergeUtils;
import com.inspur.edp.cef.designtime.api.increment.property.BooleanPropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.ObjectPropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.StringPropertyIncrement;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.entity.increment.merger.CommonModelMerger;
import com.inspur.edp.das.commonmodel.entity.object.increment.merger.CommonObjectIncrementMerger;
import java.util.HashMap;
import lombok.var;

public class BizEntityMerger extends CommonModelMerger {

  public BizEntityMerger() {
    super();
  }

  public BizEntityMerger(boolean includeAll) {
    super(includeAll);
  }

  @Override
  protected void mergeExtendInfo(GspCommonModel extendModel, CommonModelIncrement extendIncrement,
      CommonModelIncrement baseIncrement, CmControlRule rule, CmControlRuleDef def) {

    mergeAction((GspBusinessEntity) extendModel, (BizEntityIncrement) extendIncrement,
        (BizEntityIncrement) baseIncrement, rule, def);
//        if (baseIncrement.getChangeProperties().containsKey(BizEntityJsonConst.IsUsingTimeStamp)) {
//            boolean mergedTimeStamp = MergeUtils.getObjectValue(
//                    BizEntityJsonConst.IsUsingTimeStamp,
//                    (ObjectPropertyIncrement) baseIncrement.getChangeProperties().get(BizEntityJsonConst.IsUsingTimeStamp),
//                    extendIncrement.getChangeProperties(),
//                    rule);
//            ((GspBusinessEntity)extendModel).setIsUsingTimeStamp(mergedTimeStamp);
//        }
  }

  @Override
  protected void dealExtendChangeProp(GspCommonModel extendModel,
      HashMap<String, PropertyIncrement> extendIncrement, String key, PropertyIncrement increment) {

    GspBusinessEntity be = (GspBusinessEntity) extendModel;
    switch (key) {
      case BizEntityJsonConst.DependentEntityId:
        String mergeDbeId = MergeUtils.getStringValue(BizEntityJsonConst.DependentEntityId,
            (StringPropertyIncrement) increment, extendIncrement, null, null);
        be.setDependentEntityId(mergeDbeId);
        return;
      case BizEntityJsonConst.DependentEntityName:
        String mergedDbeName = MergeUtils.getStringValue(BizEntityJsonConst.DependentEntityName,
            (StringPropertyIncrement) increment, extendIncrement, null, null);
        be.setDependentEntityName(mergedDbeName);
        return;
      case BizEntityJsonConst.DependentEntityPackageName:
        String mergedDbePckName = MergeUtils
            .getStringValue(BizEntityJsonConst.DependentEntityPackageName,
                (StringPropertyIncrement) increment, extendIncrement, null, null);
        be.setDependentEntityPackageName(mergedDbePckName);
        return;
      case BizEntityJsonConst.CacheConfiguration:
        String mergedCache = MergeUtils.getStringValue(BizEntityJsonConst.CacheConfiguration,
            (StringPropertyIncrement) increment, extendIncrement, null, null);
        be.setCacheConfiguration(mergedCache);
        return;
      case BizEntityJsonConst.EnableCaching:
        boolean mergedUseCache = MergeUtils
            .getBooleanValue(BizEntityJsonConst.EnableCaching, (BooleanPropertyIncrement) increment,
                extendIncrement, null, null);
        be.setEnableCaching(mergedUseCache);
        return;
      case BizEntityJsonConst.EnableTreeDtm:
        boolean mergedTreeDtm = MergeUtils
            .getBooleanValue(BizEntityJsonConst.EnableTreeDtm, (BooleanPropertyIncrement) increment,
                extendIncrement, null, null);
        be.setEnableTreeDtm(mergedTreeDtm);
        return;
      case BizEntityJsonConst.ComponentAssemblyName:
        String mergedCompName = MergeUtils.getStringValue(BizEntityJsonConst.ComponentAssemblyName,
            (StringPropertyIncrement) increment, extendIncrement, null, null);
        be.setComponentAssemblyName(mergedCompName);
        return;
      case BizEntityJsonConst.IsUsingTimeStamp:
        boolean mergedTimeStamp = MergeUtils.getBooleanValue(BizEntityJsonConst.IsUsingTimeStamp,
            (BooleanPropertyIncrement) increment, extendIncrement, null, null);
        be.setIsUsingTimeStamp(mergedTimeStamp);
        return;
      case BizEntityJsonConst.Category:
        BECategory mergedCategory = MergeUtils
            .getObjectValue(BizEntityJsonConst.Category, (ObjectPropertyIncrement) increment,
                extendIncrement, null, null);
        be.setCategory(mergedCategory);
        return;
      case BizEntityJsonConst.ExtendType:
        String mergedExtendType = MergeUtils
            .getStringValue(BizEntityJsonConst.ExtendType, (StringPropertyIncrement) increment,
                extendIncrement, null, null);
        be.setExtendType(mergedExtendType);
        return;
      case BizEntityJsonConst.EnableApproval:
        boolean mergedEnableApproval = MergeUtils.getBooleanValue(BizEntityJsonConst.EnableApproval,
            (BooleanPropertyIncrement) increment, extendIncrement, null, null);
        be.setEnableApproval(mergedEnableApproval);
        return;
      case BizEntityJsonConst.TccSupported:
        boolean isTccSupported = MergeUtils
            .getBooleanValue(BizEntityJsonConst.TccSupported, (BooleanPropertyIncrement) increment,
                extendIncrement, null, null);
        be.setTccSupported(isTccSupported);
        return;
      case BizEntityJsonConst.AutoTccLock:
        boolean isAutoTccLock = MergeUtils
            .getBooleanValue(BizEntityJsonConst.AutoTccLock, (BooleanPropertyIncrement) increment,
                extendIncrement, null, null);
        be.setAutoTccLock(isAutoTccLock);
        return;
      case BizEntityJsonConst.AutoComplete:
        boolean isAutoComplete = MergeUtils
            .getBooleanValue(BizEntityJsonConst.AutoComplete, (BooleanPropertyIncrement) increment,
                extendIncrement, null, null);
        be.setAutoComplete(isAutoComplete);
        return;
      case BizEntityJsonConst.AutoCancel:
        boolean isAutoCancel = MergeUtils
            .getBooleanValue(BizEntityJsonConst.AutoCancel, (BooleanPropertyIncrement) increment,
                extendIncrement, null, null);
        be.setAutoCancel(isAutoCancel);
        return;
    }
  }

  //region merge actions
  private void mergeAction(GspBusinessEntity extendModel, BizEntityIncrement extendIncrement,
      BizEntityIncrement baseIncrement, CmControlRule rule, CmControlRuleDef def) {
    var extendActions = extendIncrement.getActions();
    var baseActions = baseIncrement.getActions();
    if (extendActions.size() < 1 && baseActions.size() < 1) {
      return;
    }

    for (var actionPair : baseActions.entrySet()) {
      MgrActionIncrement actionIncrement = actionPair.getValue();
      switch (actionIncrement.getIncrementType()) {
        case Added:
          mergeAddedAction(extendModel, (AddedMgrActionIncrement) actionIncrement, extendActions,
              rule, def);
          break;
        case Deleted:
          extendModel.getBizMgrActions().removeById(actionIncrement.getActionId());
          break;
        case Modify:
          dealModifyAction(extendModel, (ModifyMgrActionIncrement) actionIncrement, extendActions,
              rule, def);
      }

    }
  }


  private void mergeAddedAction(
      GspBusinessEntity extendModel,
      AddedMgrActionIncrement actionIncrement,
      HashMap<String, MgrActionIncrement> extendHelpConfigs,
      CmControlRule rule,
      CmControlRuleDef def) {

//        if (isAllowAddChildObj(rule, def))
    extendModel.getBizMgrActions().add(actionIncrement.getAction());
  }

  private void dealModifyAction(
      GspBusinessEntity extendModel,
      ModifyMgrActionIncrement baseActionIncrement,
      HashMap<String, MgrActionIncrement> extendIncrement,
      CmControlRule rule,
      CmControlRuleDef def) {
    extendModel.getBizMgrActions().removeById(baseActionIncrement.getActionId());
    extendModel.getBizMgrActions().add(baseActionIncrement.getAction());
  }
  //endregion

  @Override
  protected CommonObjectIncrementMerger getCommonObjectIncrementMerger() {
    return new BizObjectIncrementMerger(includeAll);
  }

}
