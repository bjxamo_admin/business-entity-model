/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.operation.internalmgraction;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;

/**
 * 内置Query操作
 */
public class QueryMgrAction extends BizMgrAction implements IInternalMgrAction {

  public static final String id = "m_6xIwJwoUy7qzsuZ6_S5A";
  public static final String code = "Query";
  public static final String name = "内置查询操作";

  public QueryMgrAction() {
    setID(id);
    setCode(code);
    setName(name);
  }

}
