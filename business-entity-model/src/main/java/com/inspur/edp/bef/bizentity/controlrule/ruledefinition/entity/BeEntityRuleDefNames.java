/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity;

public class BeEntityRuleDefNames {

  public static String BeEntityRuleObjectType = "GspBizObject";
  public static String AddUQConstraint = "AddUQConstraint";
  public static String ModifyUQConstraint = "ModifyUQConstraint";
  public static String AddDtermination = "AddDtermination";
  public static String ModifyDetermination = "ModifyDetermination";
  public static String AddValidation = "AddValidation";
  public static String ModifyValidation = "ModifyValidation";
}
