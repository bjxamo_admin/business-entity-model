/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.common.InternalActionUtil;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

public abstract class BizOperationSerializer<T extends BizOperation> extends JsonSerializer<T> {

  private InternalActionUtil util = new InternalActionUtil();
  protected boolean isFull = true;

  public BizOperationSerializer() {
  }

  public BizOperationSerializer(boolean full) {
    isFull = full;
  }

  @Override
  public void serialize(BizOperation value, JsonGenerator gen, SerializerProvider serializers) {
    if (isFull || !isInternalAction(value.getID())) {
      SerializerUtils.writeStartObject(gen);
      writeBaseProperty(value, gen);
      writeSelfProperty(value, gen);
      SerializerUtils.writeEndObject(gen);
    }
  }

  //region BaseProp
  private void writeBaseProperty(BizOperation op, JsonGenerator writer) {

    SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, op.getID());

    // 若为内置操作，则只序列化基本信息
    if (isInternalAction(op.getID())) {
      return;
    }
    if (op.getIsRef()) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsRef, op.getIsRef());
    }

    writeExtendOperationBaseProperty(writer, op);
  }

  private boolean isInternalAction(String opID) {
    if (util.InternalMgrActionIDs.contains(opID) || util.InternalBeActionIDs.contains(opID)) {
      return true;
    }
    return false;
  }
  //endregion

  //region SelfProp
  private void writeSelfProperty(BizOperation op, JsonGenerator writer) {
    SerializerUtils.writePropertyValue(writer, CommonModelNames.Code, op.getCode());
    SerializerUtils.writePropertyValue(writer, CommonModelNames.Name, op.getName());
    if (isFull || (op.getDescription() != null && !"".equals(op.getDescription()))) {
      SerializerUtils
          .writePropertyValue(writer, BizEntityJsonConst.Description, op.getDescription());
    }
    if (isFull || (op.getComponentId() != null && !"".equals(op.getComponentId()))) {
      SerializerUtils
          .writePropertyValue(writer, BizEntityJsonConst.ComponentId, op.getComponentId());
    }
    if (isFull || (op.getComponentName() != null && !"".equals(op.getComponentName()))) {
      SerializerUtils
          .writePropertyValue(writer, BizEntityJsonConst.ComponentName, op.getComponentName());
    }
    if (isFull || (op.getComponentPkgName() != null && !"".equals(op.getComponentPkgName()))) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ComponentPkgName,
          op.getComponentPkgName());
    }
    if (isFull || op.getIsVisible()) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsVisible, op.getIsVisible());
    }
    //SerializerUtils.writePropertyValue(writer, BizEntityConst.OpType, op.OpType);//子类赋值
    //Owner不序列化
    if (isFull || (op.getBelongModelID() != null && !"".equals(op.getBelongModelID()))) {
      SerializerUtils
          .writePropertyValue(writer, BizEntityJsonConst.BelongModelId, op.getBelongModelID());
    }
    if (isFull || !op.getIsGenerateComponent()) {
      SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsGenerateComponent,
          op.getIsGenerateComponent());
    }
    if (isFull || op.getCustomizationInfo().isCustomized()) {
      SerializerUtils
          .writePropertyValue(writer, CefNames.CustomizationInfo, op.getCustomizationInfo());
    }
    // 若为内置操作，则只序列化基本信息
    if (isInternalAction(op.getID())) {
      return;
    }
    //扩展模型属性
    writeExtendOperationSelfProperty(writer, op);
  }

  //endregion

  //region 抽象方法
  protected abstract void writeExtendOperationBaseProperty(JsonGenerator writer, BizOperation op);

  protected abstract void writeExtendOperationSelfProperty(JsonGenerator writer, BizOperation op);
  //endregion
}
