/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.beenum.BEValidationType;
import com.inspur.edp.bef.bizentity.beenum.RequestNodeTriggerType;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import java.util.EnumSet;
import java.util.HashMap;

public class BizValidationDeserializer extends BizOperationDeserializer<Validation> {

  @Override
  protected Validation createBizOp() {
    return new Validation();
  }

  @Override
  protected void beforeDeserializeBizoperation(BizOperation op) {
    Validation validation = (Validation) op;
    validation.setValidationType(BEValidationType.Consistency);
    validation.setRequestElements(new ValElementCollection());
    validation.setRequestChildElements(new HashMap<>());
    validation.setValidationTriggerPoints(new HashMap<>());
  }

  @Override
  protected boolean readExtendOpProperty(BizOperation op, String propName, JsonParser jsonParser) {
    Validation validation = (Validation) op;
    boolean hasProperty = true;
    switch (propName) {
      case BizEntityJsonConst.ValidationType:
        validation.setValidationType(SerializerUtils
            .readPropertyValue_Enum(jsonParser, BEValidationType.class, BEValidationType.values()));
        break;
      case BizEntityJsonConst.TriggerTimePointType:
        validation.setTriggerTimePointType(readBETriggerTimePointType(jsonParser));
        break;
      case BizEntityJsonConst.Order:
        SerializerUtils.readPropertyValue_Integer(jsonParser);
        break;
      case BizEntityJsonConst.PrecedingIds:
        SerializerUtils.readStringArray(jsonParser);
        break;
      case BizEntityJsonConst.SucceedingIds:
        SerializerUtils.readStringArray(jsonParser);
        break;
      case BizEntityJsonConst.RequestNodeTriggerType:
        validation.setRequestNodeTriggerType(readRequestNodeTriggerType(jsonParser));
        break;
      case BizEntityJsonConst.RequestElements:
        validation.setRequestElements(readElementArray(jsonParser));
        break;
      case BizEntityJsonConst.RequestChildElements:
        validation.setRequestChildElements(readRequestChildElements(jsonParser));
        break;
      case BizEntityJsonConst.ValidationTriggerPoints:
        validation.setValidationTriggerPoints(readValidationTriggerPoints(jsonParser));
        break;
      case BizEntityJsonConst.Parameters:
        readParameters(jsonParser, (Validation) op);
        break;
      default:
        hasProperty = false;
    }
    return hasProperty;
  }

  private HashMap<String, ValElementCollection> readRequestChildElements(JsonParser jsonParser) {
    RequestChildElementsDeserializer deserializer = new ValRequestChildElementsDeserializer();
    return deserializer.deserialize(jsonParser, null);
  }

  private ValElementCollection readElementArray(JsonParser jsonParser) {
    ValElementCollection collection = new ValElementCollection();
    SerializerUtils.readArray(jsonParser, new StringDeserializer(), collection, true);
    return collection;
  }

  private HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> readValidationTriggerPoints(
      JsonParser jsonParser) {
    ValidatoinTriggerPointsDeserializer deserializer = new ValidatoinTriggerPointsDeserializer();
    HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> dic = deserializer
        .deserialize(jsonParser, null);
    return dic;
  }

  private void readParameters(JsonParser jsonParser, Validation action) {
    BizParaDeserializer paraDeserializer = new BizActionParaDeserializer();
    BizParameterCollection<BizParameter> collection = new BizParameterCollection<>();
    SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
    action.setParameterCollection(collection);
  }
}
