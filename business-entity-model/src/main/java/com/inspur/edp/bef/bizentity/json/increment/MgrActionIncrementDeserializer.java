/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.increment;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.increment.entity.action.AddedMgrActionIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.MgrActionIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.ModifyMgrActionIncrement;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import java.io.IOException;

public class MgrActionIncrementDeserializer extends JsonDeserializer<MgrActionIncrement> {

  @Override
  public MgrActionIncrement deserialize(JsonParser jsonParser,
      DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    JsonNode node = mapper.readTree(jsonParser);
    String incrementTypeStr = node.get(CefNames.IncrementType).textValue();
    if (incrementTypeStr == null || "".equals(incrementTypeStr)) {
      return null;
    }
    IncrementType incrementType = IncrementType.valueOf(incrementTypeStr);
    switch (incrementType) {
      case Added:
        return readAddIncrementInfo(node);
      case Modify:
        return readModifyIncrementInfo(node);
      case Deleted:
//                return readDeletedIncrementInfo(node);

    }
    return null;
  }

  private AddedMgrActionIncrement readAddIncrementInfo(JsonNode node) {
    AddedMgrActionIncrement addIncrement = new AddedMgrActionIncrement();
    JsonNode addVauleNode = node.get(BizEntityJsonConst.AddedAction);
    if (addVauleNode == null) {
      return addIncrement;
    }
    BizMgrAction action = readBizOperation(addVauleNode);
    addIncrement.setAction(action);
    return addIncrement;
  }

  private ModifyMgrActionIncrement readModifyIncrementInfo(JsonNode node) {
    ModifyMgrActionIncrement modifyIncrement = new ModifyMgrActionIncrement();
    JsonNode modifyVauleNode = node.get(BizEntityJsonConst.ModifyAction);
    if (modifyVauleNode == null) {
      return modifyIncrement;
    }
    BizMgrAction action = readBizOperation(modifyVauleNode);
    modifyIncrement.setAction(action);
    return modifyIncrement;
  }

  private BizMgrAction readBizOperation(JsonNode node) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.readValue(node.toString(), BizMgrAction.class);
    } catch (IOException e) {
      throw new RuntimeException("方法反序列化失败" + node.toString(), e);
    }
  }
}
