/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizCommonDetermination;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmSerializer;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;

public class BizCommonDeterminationSerializer extends CommonDtmSerializer {

  public BizCommonDeterminationSerializer(boolean full) {
    super(full);
  }

  @Override
  protected void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info) {
    super.writeExtendCommonOpSelfProperty(writer, info);
    writeParameters(writer, (CommonDetermination) info);
    if (info instanceof BizCommonDetermination) {
      BizCommonDetermination bizCommonDetermination = (BizCommonDetermination) info;
      if (this.isFull || bizCommonDetermination.getRunOnce()) {
        SerializerUtils.writePropertyValue(writer, "RunOnce", bizCommonDetermination.getRunOnce());
      }
    }
  }

  private void writeParameters(JsonGenerator writer, CommonDetermination determination) {
    BizCommonDetermination bizCommonDetermination = (BizCommonDetermination) determination;
    if (isFull || (bizCommonDetermination.getParameterCollection() != null
        && bizCommonDetermination.getParameterCollection().getCount() > 0)) {
      SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Parameters);
      //[
      SerializerUtils.WriteStartArray(writer);
      if (bizCommonDetermination.getParameterCollection() != null
          && bizCommonDetermination.getParameterCollection().getCount() > 0) {
        for (Object item : bizCommonDetermination.getParameterCollection()) {
          new BizParameterSerializer(isFull).serialize((BizParameter) item, writer, null);
        }
      }
      //]
      SerializerUtils.WriteEndArray(writer);
    }
  }
}
