/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.increment.merger;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.increment.BizObjectIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.determination.AddedDtmIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.determination.DtmIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.validation.AddedValIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;
import com.inspur.edp.das.commonmodel.entity.object.increment.merger.CommonObjectIncrementMerger;
import java.util.HashMap;
import lombok.var;

public class BizObjectIncrementMerger extends CommonObjectIncrementMerger {

  public BizObjectIncrementMerger() {
  }

  public BizObjectIncrementMerger(boolean includeAll) {
    super(includeAll);
  }

  protected CommonObjectIncrementMerger getChildObjectMerger() {
    return new BizObjectIncrementMerger(this.includeAll);
  }

  @Override
  protected void mergeExtendObjectInfo(GspCommonObject extendObj,
      ModifyEntityIncrement extendIncrement, ModifyEntityIncrement baseIncrement,
      CommonDataTypeControlRule rule, CommonDataTypeControlRuleDef def) {

    mergeDetermination((GspBizEntityObject) extendObj, (BizObjectIncrement) extendIncrement,
        (BizObjectIncrement) baseIncrement, rule, def);
    mergeValidation((GspBizEntityObject) extendObj, (BizObjectIncrement) extendIncrement,
        (BizObjectIncrement) baseIncrement, rule, def);
  }


  //region mergeDetermination
  private void mergeDetermination(GspBizEntityObject extendObj, BizObjectIncrement extendIncrement,
      BizObjectIncrement baseIncrement, CommonDataTypeControlRule rule,
      CommonDataTypeControlRuleDef def) {
    var extendActions = extendIncrement.getDeterminations();
    var baseActions = baseIncrement.getDeterminations();
    if (extendActions.size() < 1 && baseActions.size() < 1) {
      return;
    }

    for (var actionPair : baseActions.entrySet()) {
      DtmIncrement actionIncrement = actionPair.getValue();
      switch (actionIncrement.getIncrementType()) {
        case Added:
          mergeAddedAction(extendObj, (AddedDtmIncrement) actionIncrement, extendActions, rule,
              def);
          break;
        case Deleted:
          extendObj.getDeterminations().removeById(actionIncrement.getActionId());
          break;
        case Modify:
//                    dealModifyAction(extendObj, (ModifyMgrActionIncrement)actionIncrement, extendActions, rule, def);
          break;
      }

    }
  }


  private void mergeAddedAction(
      GspBizEntityObject extendObj,
      AddedDtmIncrement actionIncrement,
      HashMap<String, DtmIncrement> extendIncrement,
      CommonDataTypeControlRule rule,
      CommonDataTypeControlRuleDef def) {

//        if (isAllowAddChildObj(rule, def))
    extendObj.getDeterminations().add(actionIncrement.getAction());
  }
  //endregion

  //region mergeDetermination
  private void mergeValidation(GspBizEntityObject extendObj, BizObjectIncrement extendIncrement,
      BizObjectIncrement baseIncrement, CommonDataTypeControlRule rule,
      CommonDataTypeControlRuleDef def) {
    var extendActions = extendIncrement.getValidations();
    var baseActions = baseIncrement.getValidations();
    if (extendActions.size() < 1 && baseActions.size() < 1) {
      return;
    }

    for (var actionPair : baseActions.entrySet()) {
      ValIncrement actionIncrement = actionPair.getValue();
      switch (actionIncrement.getIncrementType()) {
        case Added:
          mergeAddedAction(extendObj, (AddedValIncrement) actionIncrement, extendActions, rule,
              def);
          break;
        case Deleted:
          extendObj.getDeterminations().removeById(actionIncrement.getActionId());
          break;
        case Modify:
//                    dealModifyAction(extendObj, (ModifyMgrActionIncrement)actionIncrement, extendActions, rule, def);
          break;
      }

    }
  }


  private void mergeAddedAction(
      GspBizEntityObject extendObj,
      AddedValIncrement actionIncrement,
      HashMap<String, ValIncrement> extendIncrement,
      CommonDataTypeControlRule rule,
      CommonDataTypeControlRuleDef def) {

//        if (isAllowAddChildObj(rule, def))
    extendObj.getValidations().add(actionIncrement.getAction());
  }
  //endregion
}
