/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.beenum;

/**
 * 实体加锁类型
 */
public enum GspDataLockType {
  /**
   * 悲观锁：需要显式加锁成功后才能编辑数据
   */
  PessimisticLocking(0),
  /**
   * 乐观锁：提交数据时比较数据版本，如果不一致，数据作废
   */
  OptimisticLocking(1),
  /**
   * 不加锁：不保证实体数据并发一致性
   */
  None(2);

  private final int intValue;
  private static java.util.HashMap<Integer, GspDataLockType> mappings;

  private synchronized static java.util.HashMap<Integer, GspDataLockType> getMappings() {
    if (mappings == null) {
      mappings = new java.util.HashMap<Integer, GspDataLockType>();
    }
    return mappings;
  }

  GspDataLockType(int value) {
    intValue = value;
    GspDataLockType.getMappings().put(value, this);
  }

  public int getValue() {
    return intValue;
  }

  public static GspDataLockType forValue(int value) {
    return getMappings().get(value);
  }
}
