/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.beenum.AuthType;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizReturnValue;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

public abstract class BizActionBaseDeserializer extends BizOperationDeserializer<BizActionBase> {

  @Override
  protected final BizActionBase createBizOp() {
    return createBizActionBase();
  }

  @Override
  protected void beforeDeserializeBizoperation(BizOperation op) {
    BizActionBase action = (BizActionBase) op;
    action.setParameters(new BizParameterCollection());
    action.setReturnValue(new BizReturnValue());
  }

  @Override
  protected final boolean readExtendOpProperty(BizOperation op, String propName,
      JsonParser jsonParser) {
    BizActionBase action = (BizActionBase) op;
    boolean hasProperty = true;
    switch (propName) {
      case BizEntityJsonConst.CurentAuthType:
        SerializerUtils.readPropertyValue_Enum(jsonParser, AuthType.class, AuthType.values());
        break;
      case BizEntityJsonConst.OpIdList:
        action.setOpIdList(SerializerUtils.readStringArray(jsonParser));
        break;
      case BizEntityJsonConst.Parameters:
        readParameters(jsonParser, action);
        break;
      case BizEntityJsonConst.ReturnValue:
        readReturnValue(jsonParser, action);
        break;
      default:
        if (!readExtendActionProperty(jsonParser, op, propName)) {
          hasProperty = false;
        }
        break;
    }
    return hasProperty;
  }

  private void readParameters(JsonParser jsonParser, BizActionBase action) {
    BizParaDeserializer paraDeserializer = createPrapConvertor();
    BizParameterCollection<BizParameter> collection = createPrapCollection();
    SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
    action.setParameters(collection);
  }

  private void readReturnValue(JsonParser jsonParser, BizActionBase action) {
    BizReturnValueDeserializer returnValueDeserializer = new BizReturnValueDeserializer();
    BizReturnValue value = (BizReturnValue) returnValueDeserializer.deserialize(jsonParser, null);
    action.setReturnValue(value);
  }

  protected abstract BizActionBase createBizActionBase();

  protected abstract BizParaDeserializer createPrapConvertor();

  protected abstract BizParameterCollection createPrapCollection();

  protected boolean readExtendActionProperty(JsonParser jsonParser, BizOperation op,
      String propName) {
    return false;
  }
}
