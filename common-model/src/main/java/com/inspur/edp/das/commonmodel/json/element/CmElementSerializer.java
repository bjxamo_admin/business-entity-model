/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldSerializer;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.entity.element.ElementCodeRuleConfig;
import com.inspur.edp.das.commonmodel.entity.element.GspBillCodeGenerateOccasion;
import com.inspur.edp.das.commonmodel.entity.element.GspBillCodeGenerateType;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import lombok.var;

/**
 * The Json Serializer Of  CmELement
 *
 * @ClassName: VersionControlInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CmElementSerializer extends CefFieldSerializer {

    public CmElementSerializer(){}
    public CmElementSerializer(boolean full){
        super(full);
        isFull = full;
    }
    //region BaseProp
    @Override
    protected void writeExtendFieldBaseProperty(JsonGenerator writer, IGspCommonField value) {

        var field = (IGspCommonElement)value ;
        if(isFull||(field.getColumnID()!=null&&!"".equals(field.getColumnID()))){
        SerializerUtils.writePropertyValue(writer, CommonModelNames.ColumnID, field.getColumnID());}
        if(isFull||(field.getBelongModelID()!=null&&!"".equals(field.getBelongModelID()))){
        SerializerUtils.writePropertyValue(writer, CommonModelNames.BelongModelID, field.getBelongModelID());}
        writeExtendElementBaseProperty(writer, field);
    }
    //endregion

    //region SelfProp
    @Override
    protected void writeExtendFieldSelfProperty(JsonGenerator writer, IGspCommonField value) {
        var field = (IGspCommonElement)value ;
        WriteAbstractObjectInfo(writer, field);
    }

    private void WriteAbstractObjectInfo(JsonGenerator writer, IGspCommonElement field) {

        writeBillCodeConfig(writer, field.getBillCodeConfig());
        if(isFull||field.getReadonly()){
            SerializerUtils.writePropertyValue(writer, CommonModelNames.Readonly, field.getReadonly());}
        if(isFull||field.getIsCustomItem()){
            SerializerUtils.writePropertyValue(writer, CommonModelNames.IsCustomItem, field.getIsCustomItem());}
        //扩展对象属性
        writeExtendElementSelfProperty(writer, field);
    }

    private void writeBillCodeConfig(JsonGenerator writer, ElementCodeRuleConfig config) {
        if(config == null)
            return;
        if (!isFull&&!config.getCanBillCode())
            return;

        SerializerUtils.writePropertyName(writer, CommonModelNames.BillCodeConfig);
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CommonModelNames.CanBillCode, config.getCanBillCode());
        if(isFull||(config.getCodeGenerateOccasion()!=null&&config.getCodeGenerateOccasion()!= GspBillCodeGenerateOccasion.SystemProcess)){
            SerializerUtils.writePropertyValue(writer, CommonModelNames.CodeGenerateOccasion, config.getCodeGenerateOccasion().toString());
        }
        if(isFull||(config.getCodeGenerateType()!=null&&config.getCodeGenerateType()!= GspBillCodeGenerateType.none)){
            SerializerUtils.writePropertyValue(writer, CommonModelNames.CodeGenerateType, config.getCodeGenerateType().toString());
        }
        if(isFull||(config.getBillCodeName()!=null&&!"".equals(config.getBillCodeName()))){
            SerializerUtils.writePropertyValue(writer, CommonModelNames.BillCodeName, config.getBillCodeName());
        }
        if(isFull||(config.getBillCodeID()!=null&&!"".equals(config.getBillCodeID()))){
            SerializerUtils.writePropertyValue(writer, CommonModelNames.BillCodeID, config.getBillCodeID());
        }
        SerializerUtils.writeEndObject(writer);

    }
    //endregion

    //region 抽象方法
    protected abstract void writeExtendElementBaseProperty(JsonGenerator writer, IGspCommonElement element);
    protected abstract void writeExtendElementSelfProperty(JsonGenerator writer, IGspCommonElement element);

    //endregion
}
