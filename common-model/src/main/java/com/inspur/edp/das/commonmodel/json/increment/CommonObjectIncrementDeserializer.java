/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.json.increment;

import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.entity.increment.json.GspDataTypeIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeDeserializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectDeserializer;

public abstract class CommonObjectIncrementDeserializer extends GspDataTypeIncrementDeserializer {

    @Override
    protected final GspCommonDataTypeDeserializer getDataTypeDeserializer() {
        return getCommonObjectDeserializer();
    }

    protected abstract CmObjectDeserializer getCommonObjectDeserializer();

    @Override
    protected final GspFieldIncrementDeserializer getFieldSerializer() {
        return getCmElementIncrementDeserizlizer();
    }

    protected abstract CommonElementIncrementDeserializer getCmElementIncrementDeserizlizer();


    @Override
    protected final GspDataTypeIncrementDeserializer getChildEntityDeserializer() {
        return getCmObjectIncrementDeserializer();
    }

    protected abstract CommonObjectIncrementDeserializer getCmObjectIncrementDeserializer();

}
