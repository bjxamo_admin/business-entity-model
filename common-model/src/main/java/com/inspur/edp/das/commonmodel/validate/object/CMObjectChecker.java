/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.validate.object;

import com.inspur.edp.cef.designtime.api.validate.object.CommonDataTypeChecker;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;

public abstract class CMObjectChecker extends CommonDataTypeChecker {

    public final void checkObj(GspCommonObject gspCommonObject) {
        checkAllObj(this,gspCommonObject);
    }
    //遍历Child
    private void checkAllObj(CMObjectChecker cmInspaction, IGspCommonObject commonObject) {
        super.checkCDT(commonObject);
        checkAction(commonObject);
        checkExtension(commonObject);
        for (IGspCommonObject childNode : commonObject.getContainChildObjects()) {
            if (childNode == null)
                continue;
            checkAllObj(this, childNode);
        }
    }
    protected void checkAction(IGspCommonObject commonObject){

    }
    protected  void checkExtension(IGspCommonObject commonObject)
    {

    }
}
