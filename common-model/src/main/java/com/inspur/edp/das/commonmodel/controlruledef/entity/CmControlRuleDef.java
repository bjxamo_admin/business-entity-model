/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.controlruledef.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.RangeRuleDefinition;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmRuleDefSerializer;

@JsonSerialize(using = CmRuleDefSerializer.class)
@JsonDeserialize(using = CmRuleDefParser.class)
public class CmControlRuleDef extends RangeRuleDefinition {
    public CmControlRuleDef(ControlRuleDefinition parentRuleDefinition, String ruleObjectType) {
        super(parentRuleDefinition, ruleObjectType);
    }

    public ControlRuleDefItem getNameControlRule() {
        return super.getControlRuleItem(CmRuleNames.Name);
    }

    public void setNameControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CmRuleNames.Name, ruleItem);
    }

    public ControlRuleDefItem getCodeControlRule() {
        return super.getControlRuleItem(CmRuleNames.Code);
    }

    public void setCodeControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CmRuleNames.Code, ruleItem);
    }
}
