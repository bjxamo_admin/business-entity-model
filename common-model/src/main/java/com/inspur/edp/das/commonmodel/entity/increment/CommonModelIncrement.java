/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.das.commonmodel.entity.increment;

import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.AbstractIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.ModifyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.metadata.rtcustomization.api.ICustomizedContent;

import java.util.HashMap;

public class CommonModelIncrement extends AbstractIncrement implements ModifyIncrement{
    private HashMap<String, PropertyIncrement> changeProperties;
    private ModifyEntityIncrement mainEntityIncrement = new ModifyEntityIncrement();


    public IncrementType getIncrementType() {
        return IncrementType.Modify;
    }

    public HashMap<String, PropertyIncrement> getChangeProperties() {
        if (changeProperties == null)
            changeProperties = new HashMap<>();
        return changeProperties;
    }


    public ModifyEntityIncrement getMainEntityIncrement() {
        return mainEntityIncrement;
    }

    public void setMainEntityIncrement(ModifyEntityIncrement mainEntityIncrement) {
        this.mainEntityIncrement = mainEntityIncrement;
    }

}
