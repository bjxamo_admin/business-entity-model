/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.sun.xml.bind.v2.model.core.TypeRef;

class FieldSetMethodGenerator {
    private IGspCommonField field;
    private JavaClassInfo classInfo;
    private MethodInfo methodInfo = new MethodInfo();

    FieldSetMethodGenerator(IGspCommonField field, JavaClassInfo classInfo) {
        this.field = field;
        this.classInfo = classInfo;
    }

    private final String varData = "data";
    private final String varPropName = "propertyName";
    private final String varPropValue = "propertyValue";


    protected String getMethodName() {
        return "set" + field.getLabelID();
    }

    protected TypeRefInfo getReturnType() {
        return new TypeRefInfo(void.class);
    }

    public void generate() {
        methodInfo.setMethodName(getMethodName());
        methodInfo.setReturnType(getReturnType());
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);
        methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(varData, new TypeRefInfo(IEntityData.class)));
        methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(varPropValue, FieldGetMethodGenerator.getTypeInfo(field)));
        generateMethodBodies();
        classInfo.addMethodInfo(methodInfo);
    }

    private void generateMethodBodies() {
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.cef.entity.entity.EntityDataUtils");
        methodInfo.getMethodBodies().add("EntityDataUtils.setValue(data,\""+field.getLabelID()+"\",propertyValue);");
    }
}
