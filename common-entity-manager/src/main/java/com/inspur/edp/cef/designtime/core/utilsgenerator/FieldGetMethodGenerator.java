/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.entity.ClassInfo;
import com.inspur.edp.cef.designtime.api.util.MetadataUtil;
import com.inspur.edp.cef.entity.entity.AssoInfoBase;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropSet;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.eclipse.jdt.core.dom.Modifier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

class FieldGetMethodGenerator{
    private final IGspCommonField field;
    private final JavaClassInfo classInfo;
    private final String varData="data";
    private final String varPropertyName="propertyName";
    private final String getValueMethdName="getValue";
    private MethodInfo methodInfo=new MethodInfo();

    FieldGetMethodGenerator(IGspCommonField field, JavaClassInfo classInfo)
    {
        this.field = field;
        this.classInfo = classInfo;
    }

    private String getMethodName() {
        return "get"+field.getLabelID();
    }

    private TypeRefInfo getReturnType() {
       return getTypeInfo(field);
    }

    public static  TypeRefInfo getTypeInfo(IGspCommonField field)
    {
        if (field.getObjectType() == GspElementObjectType.DynamicProp)
            return new TypeRefInfo(IDynamicPropSet.class);
        else if(field.getIsUdt()) {
            UnifiedDataTypeDef udtDef = (UnifiedDataTypeDef)SpringBeanUtils
                .getBean(RefCommonService.class).getRefMetadata(field.getUdtID()).getContent();
            if (udtDef == null)
                throw new RuntimeException("找不到字段"+field.getName()+"对应的UDT元数据，元数据ID："+field.getUdtID());

            ClassInfo udtClassInfo = udtDef.getGeneratedEntityClassInfo();
            TypeRefInfo typeInfo = new TypeRefInfo(udtClassInfo.getClassName(),udtClassInfo.getClassNamespace());
            return typeInfo;
        }
        else
        {
            return getNormalTypeInfo(field);
        }
    }

    private static TypeRefInfo getNormalTypeInfo(IGspCommonField field) {
        switch (field.getObjectType()) {
            case None:
                return getNativeType(field);
            case Enum:
                return new TypeRefInfo(String.class);
            case Association:
                return new TypeRefInfo(AssoInfoBase.class);
            case DynamicProp:
            default:
                throw new RuntimeException("不支持的数据类型:"+field.getObjectType());
        }
    }

    private static TypeRefInfo getNativeType(IGspCommonField field)
    {
        switch (field.getMDataType()) {
            case String:
            case Text:
                return new TypeRefInfo(String.class);
            case Boolean:
                return new TypeRefInfo(Boolean.class);
            case Integer:
                return new TypeRefInfo(Integer.class);
            case Decimal:
                TypeRefInfo typeInfo = new TypeRefInfo(BigDecimal.class);
                return typeInfo;
            case Date:
            case DateTime:
                TypeRefInfo dateTypeInfo = new TypeRefInfo(Date.class);
                return dateTypeInfo;
            case Binary:
                TypeRefInfo binaryTypeInfo = new TypeRefInfo(byte[].class);
                return binaryTypeInfo;
            default:
                throw new RuntimeException("invalid enum values" + field.getMDataType());
        }
    }

    public void generate() {
        methodInfo.setMethodName(getMethodName());
        methodInfo.setReturnType(getReturnType());
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);
        methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(varData,new TypeRefInfo(IEntityData.class)));
        generateMethodBodies();
        classInfo.addMethodInfo(methodInfo);
    }

    private void generateMethodBodies() {
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.cef.entity.entity.EntityDataUtils");
        methodInfo.getMethodBodies().add("return ("+methodInfo.getReturnType().getTypeName()+")EntityDataUtils.getValue(data,\""+field.getLabelID()+"\");");
    }
}
