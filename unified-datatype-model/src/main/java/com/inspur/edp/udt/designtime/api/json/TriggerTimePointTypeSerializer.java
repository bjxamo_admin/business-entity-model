/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.udt.designtime.api.entity.validation.UdtTriggerTimePointType;

import java.util.EnumSet;

public class TriggerTimePointTypeSerializer extends JsonSerializer<EnumSet<UdtTriggerTimePointType>> {
	@Override
	public void serialize(EnumSet<UdtTriggerTimePointType> value, JsonGenerator writer, SerializerProvider serializers) {
		int intValue = 0;
		for (UdtTriggerTimePointType timePointType : value) {
			intValue += timePointType.getValue();
		}
		SerializerUtils.writePropertyValue_Integer(writer, intValue);
	}
}
