/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager;


import com.inspur.edp.lcm.metadata.spi.event.MetadataEventArgs;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventListener;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.manager.i18nservice.UdtI18nService;
import com.inspur.edp.udt.designtime.manager.validate.UdtValidater;

public class UdtMetadataEventListener implements MetadataEventListener {
/**
 * UDT元数据监听事件
 */
public class UdtMetadataEventListener implements MetadataEventListener {

>>>>>>> origin/dev
  public final void metadataSaving(MetadataEventArgs e) {
    // 类型判断
    if (!(e.getMetadata().getContent() instanceof UnifiedDataTypeDef)) {
      return;
    }
    UnifiedDataTypeDef udt = (UnifiedDataTypeDef) e.getMetadata().getContent();
    // 保存前校验
    new UdtValidater().validate(udt);

    //// 元数据依赖
    //      MetadataReferenceService.BuildMetadataReference(e.Metadata);
    //国际化抽取
    UdtI18nService service = new UdtI18nService();
    service.getResourceItem(e.getMetadata());
  }

  /**
   * 发布保存元数据事件
   * @param metadataEventArgs
   */
  @Override
  public void fireMetadataSavingEvent(MetadataEventArgs metadataEventArgs) {
    metadataSaving(metadataEventArgs);
  }

  @Override
  public void fireMetadataSavedEvent(MetadataEventArgs metadataEventArgs) {

  }

  @Override
  public void fireMetadataDeletingEvent(MetadataEventArgs metadataEventArgs) {

  }

  @Override
  public void fireMetadataDeletedEvent(MetadataEventArgs metadataEventArgs) {

  }
}
