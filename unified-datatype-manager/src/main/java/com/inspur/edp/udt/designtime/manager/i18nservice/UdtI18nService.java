/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager.i18nservice;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourceMergeContext;
import com.inspur.edp.lcm.metadata.api.entity.*;
import com.inspur.edp.lcm.metadata.spi.MetadataI18nService;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.i18n.ComplexUdtReourceExtractor;
import com.inspur.edp.udt.designtime.api.i18n.SimpleUdtReourceExtractor;
import com.inspur.edp.udt.designtime.api.i18n.merger.ComplexUdtResourceMerger;
import com.inspur.edp.udt.designtime.api.i18n.merger.SimpleUdtResourceMerger;
import java.io.UTFDataFormatException;

public class UdtI18nService implements MetadataI18nService {
    public GspMetadata merge(GspMetadata metadata, java.util.List<I18nResource> list) {
        if (list==null || list.size()==0)
            return metadata;
        if (!(metadata.getContent() instanceof UnifiedDataTypeDef))
            return  metadata;
        I18nResourceItemCollection resourceItems=list.get(0).getStringResources();
        if (resourceItems==null)
            return metadata;

        UnifiedDataTypeDef udt= (UnifiedDataTypeDef) metadata.getContent();
        if(udt instanceof ComplexDataTypeDef){
            mergeComplexUdtResource((ComplexDataTypeDef) udt,resourceItems);
        }
        if(udt instanceof SimpleDataTypeDef){
            mergeSimpleUdtResource((SimpleDataTypeDef) udt,resourceItems);
        }

        metadata.setContent(udt);
        return metadata;
    }

    private void mergeSimpleUdtResource(SimpleDataTypeDef udt,
        I18nResourceItemCollection resourceItems) {
            CefResourceMergeContext context=new CefResourceMergeContext(udt.getDotnetAssemblyName(),resourceItems);
            SimpleUdtResourceMerger merger=new SimpleUdtResourceMerger(udt,context);
            merger.merge();
    }

    private void mergeComplexUdtResource(ComplexDataTypeDef udt,
        I18nResourceItemCollection resourceItems) {
        CefResourceMergeContext context=new CefResourceMergeContext(udt.getDotnetAssemblyName(),resourceItems);
        ComplexUdtResourceMerger merger=new ComplexUdtResourceMerger(udt,context);
        merger.merge();
    }

    public I18nResource getResourceItem(GspMetadata metadata) {
        I18nResource resource = new I18nResource();
        resource.setResourceType(ResourceType.Metadata);
        resource.setResourceLocation(ResourceLocation.Backend);
        UnifiedDataTypeDef udt = (UnifiedDataTypeDef) metadata.getContent();
        I18nResourceItemCollection collection = new I18nResourceItemCollection();
        extractBizEntityI18NResource(udt, collection);

        resource.setStringResources(collection);
        return resource;

    }

    //region ExtractItem

    private void extractBizEntityI18NResource(UnifiedDataTypeDef udt, I18nResourceItemCollection items) {
        CefResourceExtractContext context = new CefResourceExtractContext(udt.getDotnetAssemblyName(), items);
        if (udt instanceof ComplexDataTypeDef) {
            ComplexDataTypeDef cUdt = (ComplexDataTypeDef) udt;
            ComplexUdtReourceExtractor extractor = new ComplexUdtReourceExtractor(cUdt, context);
            extractor.extract();
        } else if (udt instanceof SimpleDataTypeDef) {
            SimpleDataTypeDef sUdt = (SimpleDataTypeDef) udt;
            SimpleUdtReourceExtractor extractor = new SimpleUdtReourceExtractor(sUdt, context);
            extractor.extract();
        } else {
            throw new RuntimeException("无效的业务字段元数据类型。");
        }
    }

    //endregion
}
