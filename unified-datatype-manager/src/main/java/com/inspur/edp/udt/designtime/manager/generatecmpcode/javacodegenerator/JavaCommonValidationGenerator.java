/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator;


import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.manager.generatecomponent.JavaCompCodeNames;

/**
 * 校验规则生成器
 */
public class JavaCommonValidationGenerator extends JavaBaseCommonCompCodeGenerator {

  @Override
  protected String getBaseClassName() {

    return JavaCompCodeNames.AbstractValidationClassName;
  }

  public JavaCommonValidationGenerator(UnifiedDataTypeDef udtDef, CommonOperation operation,
      String nameSpace, String path) {
    super(udtDef, operation, nameSpace, path);
  }

  @Override
  protected String getNameSpaceSuffix() {
    return JavaCompCodeNames.UDTValidationNameSpaceSuffix;
  }

  @Override
  protected void javaGenerateExtendUsing(StringBuilder result) {

    ///#region using
    result.append(getImportStr(JavaCompCodeNames.ValidationChangeset));
    result.append(getImportStr(JavaCompCodeNames.ValidationRuntimeSpiNameSpace));
    result.append(getImportStr(JavaCompCodeNames.ValidationApiNameSpace));
    result.append(getImportStr(JavaCompCodeNames.AbstractValidationNameSpace));
    result.append(getImportStr(JavaCompCodeNames.IValidationContextNameSpace));
    //result.append(getImportStr(JavaCompCodeNames.ValidationText));

    ///#endregion
  }

  @Override
  protected void javaGenerateConstructor(StringBuilder result) {
    result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
        .append(getCompName()).append("(IValidationContext context, IChangeDetail change)")
        .append(" ").append("{").append(getNewline());

    result.append(getIndentationStr()).append(getIndentationStr()).append("super(context,change)")
        .append(";").append(getNewline());

    result.append(getIndentationStr()).append("}").append(getNewline());
  }

  @Override
  protected void javaGenerateExtendMethod(StringBuilder result) {
    result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
        .append(entityClassName).append(" ").append("getData()").append(" ").append("{")
        .append(getNewline());

    result.append(getIndentationStr()).append(getIndentationStr()).append("return").append(" ")
        .append("(").append(entityClassName).append(")").append("super.getContext().getData()")
        .append(";").append(getNewline());

    result.append(getIndentationStr()).append("}").append(getNewline());
  }

  /**
   * 获取构件名称
   */
  @Override
  protected String getInitializeCompName() {
    return String.format("%1$s%2$s", operation.getCode(), "Validation");
  }
}
