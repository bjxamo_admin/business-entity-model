/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.io.File;

public class BeCommonUtilsGenerator {

  private final GspBusinessEntity businessEntity;
  private final GspMetadata metadata;
  private final String relativePath;
  private final String compAssemblyName;

  public BeCommonUtilsGenerator(GspBusinessEntity businessEntity, GspMetadata metadata) {
    this.relativePath = metadata.getRelativePath();
    this.compAssemblyName = businessEntity.getGeneratedConfigID().toLowerCase() + ".common";
    this.businessEntity = businessEntity;
    this.metadata = metadata;
  }

  public void generateCommonUtils() {
    MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);
    ProcessMode mode = service.getProcessMode(relativePath);
    if (mode != ProcessMode.interpretation) {
      return;
    }
    String compModulePath = service.getJavaCompProjectPath(relativePath);
    File folder = new File(compModulePath);
    if (folder.exists() == false) {
      return;
    }
    for (GspBizEntityObject entityObject : businessEntity.getAllNodes()) {
      new BeEntityUtilsGenerator(entityObject, compAssemblyName, relativePath).generate();
    }
    BizMgrActionCollection bizMgrAction = businessEntity.getCustomMgrActions();
    if (bizMgrAction != null && bizMgrAction.size() > 0) {
      new BeMgrActionUtilsGenerator(businessEntity, compAssemblyName, relativePath).generate();
    }
  }
}
