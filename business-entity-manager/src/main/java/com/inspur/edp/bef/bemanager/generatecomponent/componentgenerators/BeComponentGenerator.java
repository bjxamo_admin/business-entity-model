/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizVoidReturnType;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.be.BEComponent;
import com.inspur.edp.bef.component.detailcmpentity.be.BEMethodParameter;
import com.inspur.edp.bef.component.enums.ParameterCollectionType;
import com.inspur.edp.bef.component.enums.ParameterMode;
import com.inspur.edp.bef.component.enums.ParameterType;

/**
 * 业务实体动作构件元数据生成器
 */
public class BeComponentGenerator extends BaseComponentGenerator {

  public static BeComponentGenerator getInstance() {
    return new BeComponentGenerator();
  }

  private BeComponentGenerator() {
  }

  @Override
  protected GspComponent buildComponent() {
    return new BEComponent();
  }

  ///#region 将BizAction中信息赋值给BEComponent
  @Override
  protected void evaluateComponentInfo(GspComponent component, BizOperation bizAction,
      GspComponent originalComponent) {
    this.originalComponent = originalComponent;
    //1、基本信息
    evaluateComponentBasicInfo((BEComponent) component, (BizAction) bizAction);
    //2、参数信息
    evaluateComponentParameterInfos((BEComponent) component, (BizAction) bizAction);
    //3、返回值信息
    evaluateComponentReturnValueInfo((BEComponent) component, (BizAction) bizAction);
  }

  private void evaluateComponentBasicInfo(BEComponent component, BizAction bizAction) {
    if (!CheckInfoUtil.checkNull(bizAction.getComponentId())) {
      component.setComponentID(bizAction.getComponentId());
    }
    component.setComponentCode(bizAction.getCode());
    component.setComponentName(bizAction.getName());
    component.setComponentDescription(bizAction.getDescription());
    component.getBeMethod().setDotnetAssembly(this.assemblyName);

    String suffix = String.format("%1$s%2$s%3$s", bizAction.getOwner().getCode(), '.',
        JavaCompCodeNames.ActionNameSpaceSuffix); //////
    String packageName = JavaModuleImportPackage(this.nameSpace);
    packageName = String.format("%1$s%2$s", packageName, suffix.toLowerCase());

    if (this.originalComponent != null) {
      component.getBeMethod()
          .setDotnetClassName(this.originalComponent.getMethod().getDotnetClassName());
      component.getMethod().setClassName(
          JavaModuleClassName(this.originalComponent.getMethod().getDotnetClassName(),
              packageName));
    } else {
      String classNameSuffix = this.objCode + bizAction.getCode();
      component.getBeMethod().setDotnetClassName(ComponentClassNameGenerator
          .generateBEComponentClassName(this.nameSpace, classNameSuffix));
      component.getMethod().setClassName(JavaModuleClassName(
          ComponentClassNameGenerator.generateBEComponentClassName(this.nameSpace, classNameSuffix),
          packageName));
    }

  }

  private void evaluateComponentParameterInfos(BEComponent component, BizAction bizAction) {
    for (Object parameter : bizAction.getParameters()) {
      evaluateComponentParameterInfo((IBizParameter) parameter, component);
    }
  }

  private void evaluateComponentParameterInfo(IBizParameter bizParameter, BEComponent component) {
    BEMethodParameter tempVar = new BEMethodParameter();
    tempVar.setID(bizParameter.getID());
    tempVar.setParamCode(bizParameter.getParamCode());
    tempVar.setParamName(bizParameter.getParamName());
    tempVar.setParamDescription(bizParameter.getParamDescription());
    tempVar.setMode(ParameterMode.valueOf(bizParameter.getMode().name().toString()));
    tempVar.setParameterCollectionType(ParameterCollectionType
        .valueOf(bizParameter.getCollectionParameterType().name().toString()));
    tempVar
        .setParameterType(ParameterType.valueOf(bizParameter.getParameterType().name().toString()));
    tempVar.setAssembly(bizParameter.getAssembly());
    tempVar.setClassName(bizParameter.getClassName());
    tempVar.setDotnetClassName(((BizParameter) bizParameter).getNetClassName());
    BEMethodParameter parameter = tempVar;

    component.getBeMethod().getBeParams().add(parameter);
  }

  private void evaluateComponentReturnValueInfo(BEComponent component, BizAction bizAction) {
    //基本信息
    component.getBeMethod().getReturnValue().setID(bizAction.getReturnValue().getID());
    component.getBeMethod().getReturnValue()
        .setParamDescription(bizAction.getReturnValue().getParamDescription());
    //类型
    if (bizAction.getReturnValue() instanceof BizVoidReturnType) {
      component.getBeMethod().setReturnValue((new VoidReturnType()));
    } else {
      component.getBeMethod().getReturnValue().setParameterCollectionType(ParameterCollectionType
          .valueOf(bizAction.getReturnValue().getCollectionParameterType().name().toString()));
      component.getBeMethod().getReturnValue().setParameterType(
          ParameterType.valueOf(bizAction.getReturnValue().getParameterType().name().toString()));
      component.getBeMethod().getReturnValue()
          .setAssembly(bizAction.getReturnValue().getAssembly());
      component.getBeMethod().getReturnValue()
          .setClassName(bizAction.getReturnValue().getClassName());
      component.getBeMethod().getReturnValue()
          .setDotnetClassName(bizAction.getReturnValue().getNetClassName());
    }
  }

  ///#endregion
}
