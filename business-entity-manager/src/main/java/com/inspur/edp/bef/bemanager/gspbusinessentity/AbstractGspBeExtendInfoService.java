/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.gspbusinessentity;

import com.inspur.edp.bef.bizentity.gspbusinessentity.api.IGspBeExtendInfoService;
import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfo;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * BE扩展信息服务抽象类
 *
 * @author hanll02
 */
public abstract class AbstractGspBeExtendInfoService implements IGspBeExtendInfoService {

  protected static ConcurrentHashMap<String, GspBeExtendInfo> configIdGspBeExtendInfo;

  public AbstractGspBeExtendInfoService() {
    configIdGspBeExtendInfo = new ConcurrentHashMap<>();
  }

  @Override
  public GspBeExtendInfo getBeExtendInfo(String id) {
    return null;
  }

  @Override
  public GspBeExtendInfo getBeExtendInfoByConfigId(String configId) {
    return null;
  }

  @Override
  public List<GspBeExtendInfo> getBeExtendInfos() {
    return null;
  }

  @Override
  public void saveGspBeExtendInfos(List<GspBeExtendInfo> infos) {

  }

  @Override
  public void deleteBeExtendInfo(String id) {
  }
}
