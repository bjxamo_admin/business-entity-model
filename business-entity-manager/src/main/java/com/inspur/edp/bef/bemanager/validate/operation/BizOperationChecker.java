/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bemanager.validate.operation;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.exception;
import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;

import com.inspur.edp.bef.bizentity.operation.BizOperation;

public abstract class BizOperationChecker {

  protected final void opException(String code) {
    if (code == null || code.isEmpty()) {
      return;
    }
    exception(code);
  }

  //be & deter动作编号
  private void checkActionCode(BizOperation bizOperation) {
    if (bizOperation == null) {
      return;
    }
    if (isLegality(bizOperation.getCode())) {
      return;
    }
    opException("方法编号[" + bizOperation.getCode() + "]是java关键字,请修改！");
  }

  public final void checkBizOperation(BizOperation bizOperation) {
    checkActionCode(bizOperation);
    checkBizOperationExtension(bizOperation);
  }

  protected void checkBizOperationExtension(BizOperation bizOperation) {

  }
}
